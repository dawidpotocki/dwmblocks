.POSIX:

PREFIX = /usr/local
CC = cc
CFLAGS = -std=c99 -Wall -Wpedantic -D _DEFAULT_SOURCE

dwmblocks: dwmblocks.o
	${CC} dwmblocks.o -lX11 -o dwmblocks
dwmblocks.o: dwmblocks.c config.h
	${CC} ${CFLAGS} -c dwmblocks.c

clean:
	rm -f *.o *.gch dwmblocks

install: dwmblocks
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f dwmblocks $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/dwmblocks

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/dwmblocks

.PHONY: clean install uninstall
